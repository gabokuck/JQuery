$(document).ready(function(){

    $("#boton1").click(function(){
        // Para ver el tipo de atributo
        var x = "El valor del atributo es: " + $("input").attr("type");
        // Para ver el ID del atributo
        var y = "El valor del atributo es: " + $("input").attr("id");
        $("#info").html(x);
        $("#info2").html(y);
    });
    // Cambiar atributo
    $("#boton2").click(function(){
        $("input").attr("id", "codigo");
    });

    // Cambiar un solo atributo
    $("#boton3").click(function(){
        // en attr("nombre de la etiqueta", "nombre por el que se va a acambiar")
        $("input[type='text']").attr("id", "codigo2");
    });

});