$(document).ready(function(){
    // .click() es para cuando se haga click en este elemento se ejecute la funcion
    $("#boton").click(function(){
        // con this esto llama automaticamente con el selector que estemos trabajando
        $(this).hide(3000).show(500);
    });
});