$(document).ready(function(){
    // .hide() sirve para ocultar y .show() para mostrar
    // los numero que estan dentro de los parentesis es el tiempo
    // por ejemplo 3000 son 3 segundos
    $("#codigo").hide(3000).show(1000);
});